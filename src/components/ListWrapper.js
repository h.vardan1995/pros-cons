import React from "react";

import List from "./List";

import Styles from "./common.module.scss";

const ListWrapper = ({ list, setList, title }) => {
  return (
    <div className={Styles.ListWrapper}>
      <p>{title}</p>
      {list.map((el, i) => {
        return (
          <List
            key={i}
            title={el}
            id={i + 1}
            handleChange={(e) => {
              const value = e.target.value;
              setList((prevState) => {
                const nextState = [...prevState];
                nextState[i] = value;
                if (value && i + 1 === prevState.length) {
                  nextState.push("");
                } else if (value.length === 0) {
                  nextState.pop("");
                }
                return nextState;
              });
            }}
          />
        );
      })}
    </div>
  );
};

export default React.memo(ListWrapper);
