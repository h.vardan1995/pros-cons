import React, { useState } from "react";

import ListWrapper from "./ListWrapper";
import Styles from "./common.module.scss";

const MainWrapper = ({ title }) => {
  const [pros, setPros] = useState([""]);
  const [cons, setcons] = useState([""]);

  return (
    <div style={Styles.MainWrapper}>
      <div className={Styles.Title}>
        <p>{title}</p>
      </div>
      <div className={Styles.Section}>
        <ListWrapper list={pros} setList={setPros} title="PROS" />
        <ListWrapper list={cons} setList={setcons} title="CONS" />
      </div>
    </div>
  );
};

export default React.memo(MainWrapper);
