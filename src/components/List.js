import React from "react";
import classNames from "classnames";

import Styles from "./common.module.scss";

const List = ({ title, id, handleChange }) => {
  return (
    <div className={Styles.List}>
      <span className={classNames({ [Styles.IdIsgreater]: id >= 10 })}>
        {id}
      </span>
      <input onChange={handleChange} value={title} />
    </div>
  );
};

export default React.memo(List);
